package io.domonkos.exchange_sample.data_access;

import java.time.LocalDate;

public interface RateStore {
	
	public void store(String fromCurrency, String toCurrency, LocalDate fromDate, double rate);
	
	// fetches the latest
	public Double getRate(String fromCurrency, String toCurrency);
	
	// fetches from a particular date
	public Double getRate(String fromCurrency, String toCurrency, LocalDate fromDate);
	
	public LocalDate getNewestDate();
	
	public LocalDate getOldestDate();
}
