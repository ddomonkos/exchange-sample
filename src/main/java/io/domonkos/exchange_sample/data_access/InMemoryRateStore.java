package io.domonkos.exchange_sample.data_access;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.domonkos.generics.Pair;

public class InMemoryRateStore implements RateStore {
	
	private static InMemoryRateStore instance;
	
	public static InMemoryRateStore getInstance() {
		if (instance == null) {
			instance = new InMemoryRateStore();
		}
		return instance;
	}
	
	
	public static final int HISTORY_SIZE = 90; // in days
	private Map<LocalDate, Map<Pair<String, String>, Double>> store = new HashMap<>();

	public void store(String fromCurrency, String toCurrency, LocalDate fromDate, double rate) {
		if (fromDate == null) {
			throw new IllegalArgumentException();
		}
		
		Map<Pair<String, String>, Double> dayMap = store.get(fromDate);
		if (dayMap == null) {
			dayMap = new HashMap<>();
		}
		
		dayMap.put(new Pair<>(
			processCurrencyString(fromCurrency),
			processCurrencyString(toCurrency)
		), rate);
		
		store.put(fromDate, dayMap);
	}

	public Double getRate(String fromCurrency, String toCurrency) {
		LocalDate newestDate = getNewestDate();
		if (newestDate == null) {
			return null;
		} else {
			return store.get(newestDate).get(new Pair<>(fromCurrency, toCurrency));
		}
	}

	public Double getRate(String fromCurrency, String toCurrency, LocalDate fromDate) {
		if (fromDate == null) {
			return getRate(fromCurrency, toCurrency);
		}
		
		Map<Pair<String, String>, Double> dayMap = store.get(fromDate);
		if (dayMap == null) {
			return null;
		} else {
			return dayMap.get(new Pair<>(fromCurrency, toCurrency));
		}
	}
	
	protected String processCurrencyString(String currency) {
		if (currency == null || currency.length() != 3) {
			throw new IllegalArgumentException();
		}
		return currency.toLowerCase();
	}

	@Override
	public LocalDate getNewestDate() {
		List<LocalDate> dates = getSortedDates();
		return dates.isEmpty() ? null : dates.get(dates.size() - 1);
	}

	@Override
	public LocalDate getOldestDate() {
		List<LocalDate> dates = getSortedDates();
		return dates.isEmpty() ? null : dates.get(0);
	}
	
	public void purgeOldRecords() {
		LocalDate oldestDate = getOldestDate();
		LocalDate now = LocalDate.now();
		if (oldestDate != null && oldestDate.plusDays(HISTORY_SIZE).isBefore(now)) {
			store.keySet().forEach((LocalDate date) -> {
				if (date.plusDays(HISTORY_SIZE).isBefore(now)) {
					store.remove(date);
				}
			});
		}
	}

	protected List<LocalDate> getSortedDates() {
		List<LocalDate> list = new ArrayList<LocalDate>(store.keySet());
		Collections.sort(list);
		return list;
	}

}
