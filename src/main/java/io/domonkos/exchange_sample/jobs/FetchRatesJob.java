package io.domonkos.exchange_sample.jobs;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.domonkos.exchange_sample.data_access.InMemoryRateStore;
import io.domonkos.exchange_sample.data_access.RateStore;

@Component
public class FetchRatesJob {

	public static final String FROM_CURRENCY = "eur";
	public static final String DAILY_URL = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	public static final String HISTORY_URL = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";
	
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Scheduled(fixedRate = 60 * 60 * 1000) // every hour
    public void perform() {
    	RateStore store = InMemoryRateStore.getInstance();
    	LocalDate oldestDate = store.getOldestDate();
    	if (oldestDate == null || oldestDate.isAfter(LocalDate.now().minusDays(InMemoryRateStore.HISTORY_SIZE))) {
    		fetchHistory();
    	} else if (store.getNewestDate().isBefore(LocalDate.now())) {
    		fetchToday();
    	}
    	
    	((InMemoryRateStore) store).purgeOldRecords();
    }
    
    protected void fetchToday() {
    	Document document = fetchDocument(DAILY_URL);
    	if (document != null) {
    		processDocument(document);
    		log.info("fetched today's rates");
    	}
    }
    
    protected void fetchHistory() {
    	Document document = fetchDocument(HISTORY_URL);
    	if (document != null) {
    		processDocument(document);
    		log.info("fetched history rates");
    	}
    }
    
    protected void processDocument(Document document) {
    	RateStore store = InMemoryRateStore.getInstance();
		List<Element> dayElements = document.selectNodes("/*/*[name()='Cube']/*[name()='Cube']");
		
		if (dayElements.isEmpty()) {
			log.error("didn't find any entries, please check the XML that is returned by the ECB");
		}
		
		dayElements.forEach((Element dayElement) -> {
			List<Element> rateElements = dayElement.selectNodes("./*[name()='Cube']");
			LocalDate date = LocalDate.parse(dayElement.attributeValue("time"));
			rateElements.forEach((Element rateElement) -> {
				store.store(
					FROM_CURRENCY,
					rateElement.attributeValue("currency"),
					date,
					Double.parseDouble(rateElement.attributeValue("rate"))
				);
			});
		});
    }
    
    protected Document fetchDocument(String url) {
    	HttpClient client = HttpClients.createDefault();
    	HttpGet request = new HttpGet(url);
    	SAXReader reader = new SAXReader();
    	
    	try {
			HttpResponse response = client.execute(request);
			int code = response.getStatusLine().getStatusCode();
			if (code == 200) {
				return reader.read(response.getEntity().getContent());
			} else {
				log.error("got status code " + code + " when accessing " + url);
				return null;
			}
		} catch (IOException|UnsupportedOperationException|DocumentException e) {
			log.error("error accessing " + url + ": " + e);
			return null;
		}
    }
}