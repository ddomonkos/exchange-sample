package io.domonkos.exchange_sample.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.domonkos.exchange_sample.data_access.InMemoryRateStore;
import io.domonkos.exchange_sample.data_access.RateStore;

@Controller
public class MainController {

    private static final String FROM_CURRENCY = "eur";

    @ResponseBody
    @RequestMapping(value = "/api/" + FROM_CURRENCY + "/{to}", produces = "application/json")
    public ResponseEntity<String> getRate(
    		@PathVariable("to") String toCurrency,
    		@RequestParam(value = "date", required = false) String strDate) {
    	
    	LocalDate date = null;
    	if (strDate != null) {
    		try {    			
    			date = LocalDate.parse(strDate);
    		} catch (DateTimeParseException e) {
    			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null); 
    		}
    	}
    	
    	RateStore store = InMemoryRateStore.getInstance();
    	Double rate = store.getRate(FROM_CURRENCY, toCurrency, date);
    	if (rate == null) {
    		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    	} else {
    		return ResponseEntity.ok("{\"exchange-rate\": " + rate + "}");
    	}
    }
}
